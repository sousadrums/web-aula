<?php
require_once 'models/grupo.php';

class grupoController{

	public function grupos(){
		Utils::isAdmin();
		$grupo=new Grupo();
		$grupos=$grupo->grupos();
		$grupocon=$grupo->grupoCon();
		if (isset($_SESSION['admin'])){
			 require_once 'views/profesor/admin.php';
		}
		require_once 'views/grupo/gruposin.php';
		require_once 'views/grupo/grupos.php';

	}

	public function asignar(){
		Utils::IsAdmin();
		if (isset($_POST)) {
			$grupo = isset($_POST['grupo']) ? $_POST['grupo'] : false;
			$aula=isset($_POST['aula']) ? $_POST['aula'] : false;
			$id_tutor=$_GET['id'];

			if($grupo && $aula && $id_tutor){
				$grupot= new Grupo();
				$grupot->setId($grupo);
				$nombreg=$grupot->grupoDesdeId();
				$grupot->setGrupo($nombreg);
				$grupot->setId_tutor($id_tutor);
				$grupot->setId_aula($aula);
				$save1=$grupot->tutorGrupo();
				$save2=$grupot->tutoria();
				
				


				

				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
	header("Location:".base_url.'profesor/profesores');

	}


	public function register(){
		if (isset($_SESSION['admin'])){
			 require_once 'views/profesor/admin.php';
			 require_once 'views/grupo/crear.php';
		}
	}

	public function save(){
		if (isset($_POST)) {
			
			$grupon = isset($_POST['grupo']) ? $_POST['grupo'] : false;


			if($grupon){
				$cero=0;
				$grupo= new Grupo();
				$grupo->setGrupo($grupon);
				$grupo->setId_tutor($cero);
				
				$save= $grupo->save();
				
				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		header("Location:".base_url.'grupo/grupos');
	}


	public function borrar(){
		Utils::isAdmin();
			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$grupo= new Grupo();
				$grupo->setId($id);
				$delete=$grupo->borrarGrupo();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."grupo/grupos");
	}
}

?>