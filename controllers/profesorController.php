<?php
require_once 'models/profesor.php';

class profesorController{

	public function index(){
		require_once 'views/profesor/login.php';
		
	}

	public function register(){
		if (isset($_SESSION['admin'])){
			 require_once 'views/profesor/admin.php';
		}
		require_once 'views/profesor/registro.php';
	}

	public function loginV(){
		require_once 'views/profesor/login.php';
	}
	public function loged(){

		if (isset($_SESSION['admin'])){
			 require_once 'views/profesor/admin.php';
		}elseif(isset($_SESSION['tutor'])){
			require_once 'views/profesor/tutor.php';
		}elseif(isset($_SESSION['identity'])){
				require_once 'views/profesor/profe.php';
		}else{
				require_once 'views/profesor/login.php';
				echo '<div class="alert alert-danger">Usuario y/o contraseña incorrectos. Introduce de nuevo tus credenciales.</div>';
		}
	}

	public function profesores(){
		Utils::isAdmin();
		$profesor=new Profesor();
		$profesores=$profesor->getAll();
		$gruposin=$profesor->grupoSin();
		$aulasin=$profesor->aulasin();
		
		if (isset($_SESSION['admin'])){
			 require_once 'views/profesor/admin.php';
		}
		require_once 'views/profesor/profesores.php';
	}

	public function tutores(){
		Utils::isAdmin();
		$profesor=new Profesor();
		$profesores=$profesor->getTutores();
		if (isset($_SESSION['admin'])){
			 require_once 'views/profesor/admin.php';
		}
		require_once 'views/profesor/tutores.php';
	}


	public function login(){
		if(isset($_POST)){
			// Identificar al usuario
			// Consulta a la base de datos
			$profesor = new Profesor();
			$profesor->setUsuario($_POST['usuario']);
			$profesor->setContraseña($_POST['contraseña']);
			$identity = $profesor->login();
			var_dump($identity);



			if($identity && is_object($identity)){
				$_SESSION['identity'] = $identity;
				
				if($identity->rol == 'admin'){
					$_SESSION['admin'] = true;
				}elseif($identity->rol == 'tutor'){
					$_SESSION['tutor'] = true;
				}else{
					$_SESSION['profe']=true;
				}
				
			}else{
				$_SESSION['error_login'] = 'Identificación fallida !!';
			}
		
		}
		header("Location:".base_url."profesor/loged");
	}
	
	public function logout(){
		if(isset($_SESSION['identity'])){
			unset($_SESSION['identity']);
		}
		
		if(isset($_SESSION['admin'])){
			unset($_SESSION['admin']);
		}
		if(isset($_SESSION['tutor'])){
			unset($_SESSION['tutor']);
		}
		if(isset($_SESSION['profe'])){
			unset($_SESSION['profe']);
		}
		
		header("Location:".base_url."profesor/loginV");
	}

	public function save(){
		if (isset($_POST)) {
			
			$usuario = isset($_POST['usuario']) ? $_POST['usuario'] : false;
			$contraseña = isset($_POST['contraseña']) ? $_POST['contraseña'] : false;
			$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
			$apellido = isset($_POST['apellido']) ? $_POST['apellido'] : false;

			
			if($usuario && $contraseña && $nombre && $apellido){
				$profesor= new Profesor();
				$profesor->setUsuario($usuario);
				$profesor->setContraseña($contraseña);
				$profesor->setNombre($nombre);
				$profesor->setApellido($apellido);
				
				$save= $profesor->save();

				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		header("Location:".base_url.'profesor/register');


	}

	public function edit(){
		if (isset($_SESSION)) {
			$usuario=$_SESSION['identity']->usuario;
			$nombre=$_SESSION['identity']->nombre;
			$apellido=$_SESSION['identity']->apellido;
	
			if (isset($_SESSION['admin'])) {
				require_once 'views/profesor/admin.php';	
			}elseif(isset($_SESSION['tutor'])){
				require_once 'views/profesor/tutor.php';
			}else{
				require_once 'views/profesor/profe.php';
			}
			require_once 'views/profesor/eduser.php';

		}
	}


	public function borrar(){
		Utils::isAdmin();
			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$profesor= new Profesor();
				$profesor->setId($id);
				$desasig=$profesor->quitarTutoria();
				$delete=$profesor->borrarProfe();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."profesor/profesores");
	}

	public function update(){
		if (isset($_POST)) {
			$profesor = new Profesor();
			$profesor->setId($_SESSION['identity']->id);
			$profesor->setUsuario($_POST['usuario']);
			$profesor->setNombre($_POST['nombre']);
			$profesor->setApellido($_POST['apellido']);
			
			$save=$profesor->edit();

			session_unset();


			if ($save) {
				$_SESSION['profesor']="complete";
			}else{
				$_SESSION['profesor']="failed";
			}

		}else{
			$_SESSION['profesor']="failed";
		}
		header("Location:".base_url);
	}

	public function contraseña(){
		if (isset($_SESSION['admin'])) {
				require_once 'views/profesor/admin.php';
			}elseif(isset($_SESSION['tutor'])){
				require_once 'views/profesor/tutor.php';
			}else{
				require_once 'views/profesor/profe.php';
			}
		require_once 'views/profesor/ccontraseña.php';
	}
	public function ccontraseña(){
		if (isset($_POST)) {
			$old=$_POST['old'];
			$new1=$_POST['new1'];
			$new2=$_POST['new2'];
			$verify1 = password_verify($old, $_SESSION['identity']->contraseña);

			$verify2=false;
			if ($new1==$new2) {
				$verify2=true;
			}

			if (!$verify1) {
				$this->contraseña();
				echo '<div class="alert alert-danger">La contraseña no es correcta</div>';
				
			}elseif (!$verify2) {
				$this->contraseña();
				echo '<div class="alert alert-danger">Las contraseñas no coinciden</div>';

			}else{
				$profesor = new Profesor();
				$profesor->setId($_SESSION['identity']->id);
				$profesor->setContraseña($new2);
				$save=$profesor->updateContraseña();
				
				if ($save) {
					$_SESSION['uPassword']="complete";
					//$this->contraseña();
					//echo '<div class="alert alert-success">La contraseña se ha actualizado correctamente</div>';
					require_once 'views/profesor/cambiopassword.php';
				}else{
					$_SESSION['uPassword']="failed";
				}

			}

		}
		
		
	}

	public function asignarTutoria(){
		Utils::isAdmin();
		$id=$_GET['id'];
		$tutor=new Profesor();
		$id=$tutor->setId($id);
		$tutor->aTutor();

		require_once 'views/profesor/admin.php';	
		header("Location:".base_url."profesor/profesores");
	}

}

