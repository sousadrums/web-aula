<?php 

require_once 'models/aula.php';


class aulaController{



	public function ver(){
		$aula=new aula();
		$aula->setId($_GET['id']);
		$pupitres=$aula->getPupitres();

		if (isset($_SESSION['admin'])) {
				require_once 'views/profesor/admin.php';
			}elseif(isset($_SESSION['tutor'])){
				require_once 'views/profesor/tutor.php';
			}else{
				require_once 'views/profesor/profe.php';
			}
		require_once 'views/aula/aula.php';
		
	}

	public function aulas(){
		Utils::isAdmin();
		$aula=new Aula();
		$aulas=$aula->aulas();
		if (isset($_SESSION['admin'])) {
				require_once 'views/profesor/admin.php';
			}
		require_once 'views/aula/aulas.php';
	}

	function crear(){
			if (isset($_SESSION['admin'])){
			 require_once 'views/profesor/admin.php';
			 require_once 'views/aula/crear.php';
		}
	}

	public function save(){
		if (isset($_POST)) {
			
			$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
			$columnas = isset($_POST['columnas']) ? $_POST['columnas'] : false;
			$filas = isset($_POST['filas']) ? $_POST['filas'] : false;


			if($nombre && $columnas && $filas){
				$aula= new Aula();
				$aula->setNombre($nombre);
				$aula->setColumnas($columnas);
				$aula->setFilas($filas);
				
				$save= $aula->save();
			
				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		
		header("Location:".base_url.'aula/aulas');
	}

	public function borrar(){
		Utils::isAdmin();
			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$aula= new Aula();
				$aula->setId($id);
				$delete=$aula->borrarAula();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."aula/aulas");
	}




}

?>