<div class="col-sm-8">	
	<div  class="jumbotron">
		<h1>Cambiar contraseña</h1>
		<form action="<?=base_url?>profesor/ccontraseña" method="POST">
			<div class="form-group">
				<input type="password" class="form-control" name="old" placeholder="Contraseña antigua" required /><br>
				<input type="password" class="form-control" name="new1" placeholder="Contraseña nueva" required /><br>
				<input type="password" class="form-control" name="new2" placeholder="Repite la contraseña nueva" required /><br>
				<input type="submit" class="btn btn-primary" value="Cambiar" />
			</div>
		</form>
	</div>
</div>