<div class="col">	
	<div class="jumbotron">
		<?php if (isset($_SESSION['register']) && $_SESSION['register'] == 'complete'): ?>
			<div class="alert alert-success">Registro completado correctamente</div>
	<?php elseif(isset($_SESSION['register']) && $_SESSION['register'] == 'failed'): ?>
			<div class="alert alert-danger">Registro fallido</div>
	<?php endif; ?>
	<?php Utils::deleteSession('register'); ?>

		<h1>Registrar nuevo usuario</h1>
		<form action="<?=base_url?>profesor/save" method="POST">
				<input class="form-control" placeholder="usuario" type="text" name="usuario" required /><br>
				<input class="form-control" placeholder="contraseña" type="password" name="contraseña" required /><br>
				<input class="form-control" placeholder="nombre" type="text" name="nombre" required /><br>
				<input class="form-control" placeholder="apellido" type="text" name="apellido" required /><br>
				<input type="submit" class="btn btn-primary" value="Registrar" />
		</form>
	</div>
</div>