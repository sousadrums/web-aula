<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Profesores</h5>
		<a class="btn btn-primary mb-2" href="<?=base_url?>profesor/register">Añadir</a>
		<table class="table table-hover table-dark table-responsive ">
			<thead>
				<tr>
					<th>Profesor</th>
					<th>rol</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php

				 while ($profe=$profesores->fetch_object()) : ?>
					
					<tr>
						<td><?=$profe->nombre; ?> <?=$profe->apellido; ?></td>
						<td><?=$profe->rol; ?></td>
						<td class="row">
							<a class="btn btn-primary btn-sm h-1 <?php if($profe->rol=='tutor'){echo 'disabled'; } ?>" href="<?=base_url?>profesor/asignarTutoria&id=<?=$profe->id?>">tutor</a>
							<?php if($profe->rol=='tutor' && $profe->id_tutor==0) : ?>	
								<form action="<?=base_url?>grupo/asignar&id=<?=$profe->id?>" method="POST" class="form-group">
									<select name="grupo" class="form-control-sm">
									<?php while ($grupo=$gruposin->fetch_object()) : ?>
										<option value="<?=$grupo->id?>"><?=$grupo->grupo?>
										</option>
									<?php endwhile; ?>
									</select>
									<select name="aula" class="form-control-sm">
									<?php while ($aula=$aulasin->fetch_object()) : ?>
										<option value="<?=$aula->id?>"><?=$aula->nombre?>
										</option>
									<?php endwhile; ?>
									</select>
									<button type="submit" class="btn btn-primary btn-sm">asignar</button>
								</form>
							<?php endif; ?>
						<a class="btn btn-danger btn-sm" href="<?=base_url?>profesor/borrar&id=<?=$profe->id?>">Eliminar</a>
						</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>		
	</div>
</div>
<br>
