<div class="col" >
	<div class="container pt-3 pb-3 border">
		<h5>Alumnos</h5>
		<table class="table table-hover table-dark table-responsive-sm">
			<thead>
				<tr>
					<th>Nombre y apellidos</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($grupo=$grupos->fetch_object()) : ?>
					<tr>
						<td><?=$grupo->grupo; ?></td>
						<td><a class="btn btn-danger btn-sm" href="<?=base_url?>grupo/borrar&id=<?=$grupo->id?>">Eliminar</a></td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>
		<a class="btn btn-primary mb-2" href="<?=base_url?>grupo/register">Añadir</a>		
	</div>
</div>
<br>
