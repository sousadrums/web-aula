<div id="cursos">
		<h2>Registros <?=$nombre; ?></h2>
	<br/>
<?php if (isset($_SESSION['delete']) && $_SESSION['delete'] == 'complete'): ?>
		<strong>Registro borrado correctamente</strong>
<?php elseif(isset($_SESSION['delete']) && $_SESSION['delete'] == 'failed'): ?>
		<strong>Borrado fallido.</strong>
<?php endif; ?>
<?php Utils::deleteSession('delete'); ?>
<br>
	<table>
		<tr>
			<th>fecha</th>
			<th>hora</th>
			<th>Peso</th>
			<th> % Grasa</th>
			<th>% Grasa visc.</th>
			<th>% Musculo</th>
			<th>calorias</th>
			<th>pliegue tripa</th>
			<th>perimetro cintura</th>
			<th>acciones</th>
		</tr>
		<?php while ($reg=$registros->fetch_object()) : ?>
			<tr>
				<td><?=$reg->fecha; ?></td>
				<td><?=$reg->hora; ?></td>
				<td><?=$reg->peso; ?></td>
				<td><?=$reg->grasa; ?></td>
				<td><?=$reg->grasav; ?></td>
				<td><?=$reg->musculo; ?></td>
				<td><?=$reg->calorias; ?></td>
				<td><?=$reg->plieguem; ?></td>
				<td><?=$reg->cintura; ?></td>
				<td>
					<a href="<?=base_url?>peso/detalles&id=<?=$reg->id?>" class="button button">Detalles</a>
					<?php if (!isset($_SESSION['nutri'])): ?>
						<a href="<?=base_url?>peso/delete&id=<?=$reg->id?>" class="button button-danger">Eliminar</a>
					<?php endif; ?>
				</td>
			</tr>
		<?php endwhile; ?>
	</table>
</div>