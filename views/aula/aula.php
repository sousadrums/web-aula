<div class="col">
	<div class="container border center">	
	<div class="container p-5 border">
			<?php $pupitre=$pupitres->fetch_object() ?>
				<h1>Aula <?=$pupitre->nombre?></h1>
				<div class="row row-cols-<?=$pupitre->columnas?>">
					<?php 
					$columna=1; 
					while ($columna<=$pupitre->columnas) : ?>
						<div class="col">
						<?php 
						$fila=1;
						while ($fila<=$pupitre->filas) : ?>
							<div class="container border px-0 bg-info mb-3 ">
								<div class="d-flex justify-content-between">
									<div class=" bg-success  border border-left-0 border-top-0"><?=$fila?><?=$columna?></div>
									<div class="pr-3 border border-right-0 border-top-0 bg-danger"></div>
								</div>
								<div class="row pb-4">
									<div class="col text-center">Jorge Lirio sousa</div>
								</div>

							</div>
						<?php $fila++ ?>
						<?php endwhile ; ?>
						<?php $columna++; ?>
						</div>
					<?php endwhile ; ?>
				</div>
				<br>
			<div class="row">
				<div class="col"></div>
				<div class="col bg-secondary text-light text-center border"><strong>Pizarra</strong></div>
				<div class="col"></div>
			</div>
		</div>
	</div>
</div>