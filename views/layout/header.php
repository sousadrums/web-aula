
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no"/>
	<title>MI AULA</title>
	<link rel="icon" type="image/x-icon" href="<?=base_url?>assets/img/logo.png" />
	<!--<link rel="stylesheet" type="text/css" href="<?=base_url?>assets/css/styles.css" />-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script>

function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(data));
}
</script>
</head>
<body>
	<div class="container">
		<!--Cabecera-->
		<nav class="navbar  navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="#">MI AULA</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
   				<span class="navbar-toggler-icon"></span>
  			</button>
  			<div class="collapse navbar-collapse" id="collapsibleNavbar">
				<ul class="navbar-nav">
					<?php if(!isset($_SESSION['identity'])): ?>
						<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/loginV">Inicio sesión</a></li>
					<?php else: ?>
						<li class="nav-item"><a class="nav-link" href="#"><?=$_SESSION['identity']->nombre?> <?=$_SESSION['identity']->apellido?> </a></li>
						
						<li class="nav-item"><a class="nav-link" href="<?=base_url?>profesor/logout">Cerrar sesión</a></li>
					<?php endif; ?>
					<li class="nav-item"><a class="nav-link" href="#">ayuda</a></li>
				</ul>
			</div>
		</nav>
	<br>
	<div class="row">