<?php

class Profesor{
	private $id;
	private $usuario;
	private $contraseña;
	private $nombre;
	private $apellido;
	private $rol;
	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}

	function getId(){
		return $this->id;
	}
	function getUsuario(){
		return $this->usuario;
	}
	function getContraseña(){
		return  password_hash($this->db->real_escape_string($this->contraseña), PASSWORD_BCRYPT, ['cost'=> 4]);
	}
	function getNombre(){
		return $this->nombre;
	}
	function getApellido(){
		return $this->apellido;
	}
	function getRol(){
		return $this->rol;
	}
	

	function setId($id){
		$this->id=$id;
	}
	function setUsuario($usuario){
		$this->usuario=$this->db->real_escape_string($usuario);
	}
		function setContraseña($contraseña){
		$this->contraseña= $contraseña;
	}
	function setNombre($nombre){
		$this->nombre=$this->db->real_escape_string($nombre);
	}
	function setApellido($apellido){
		$this->apellido=$this->db->real_escape_string($apellido);
	}
	function setRol($rol){
		$this->rol=$rol;
	}

	function getAlll(){
		$sql="SELECT * FROM profesor WHERE rol!='admin'";
		$profesores= $this->db->query($sql);
		return $profesores;

	}

	function getAll(){
		$sql="SELECT * FROM grupo g RIGHT JOIN profesor p ON g.id_tutor=p.id WHERE rol!='admin'";
		$profesores= $this->db->query($sql);
		return $profesores;
	}

	function getTutores(){
		$sql="SELECT p.id id, p.nombre nombre, p.apellido apellido, p.rol rol, g.grupo grupo FROM grupo g INNER JOIN profesor p ON g.id_tutor=p.id WHERE p.rol='tutor'ORDER BY p.id ASC ";
		$tutores= $this->db->query($sql);
		return $tutores;
	}
	


	public function save(){
		$sql = "INSERT INTO profesor VALUES(NULL, '{$this->getUsuario()}', '{$this->getContraseña()}', '{$this->getNombre()}', '{$this->getApellido()}', 'profe')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function login(){
		$result = false;
		$usuario = $this->usuario;
		$contraseña = $this->contraseña;
		
		// Comprobar si existe el usuario
		$sql = "SELECT * FROM profesor WHERE usuario = '$usuario'";
		$login = $this->db->query($sql);
		

		if($login && $login->num_rows == 1){
			$profesor = $login->fetch_object();
			
			// Verificar la contraseña
			$verify = password_verify($contraseña, $profesor->contraseña);
			if($verify){
				$result = $profesor;
			}
		}

		return $result;
	}

	public function aTutor(){
		$sql="UPDATE profesor SET rol='tutor' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;

	}
	

	public function aProfe(){
		$sql="UPDATE profesor SET rol='profe' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;

	}

	public function edit(){
		$sql="UPDATE profesor SET usuario='{$this->getUsuario()}', nombre='{$this->getNombre()}', apellido='{$this->getApellido()}' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function updateContraseña(){
		$sql="UPDATE profesor SET contraseña='{$this->getContraseña()}' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;

	}

		public function grupoSin(){
		$sql="SELECT * FROM grupo WHERE id_tutor=0";
		$gruposin= $this->db->query($sql);
		return $gruposin;
	}

	function aulasin(){
		$sql="SELECT a.nombre, a.id  FROM aula a LEFT JOIN asignatura s ON a.id<>s.id_aula";
		$aulasin= $this->db->query($sql);
		return $aulasin;
	}


	
	public function quitarTutoria(){
		$sql="UPDATE grupo SET id_tutor=0 WHERE id_tutor={$this->id}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

	public function borrarProfe(){
		$sql="DELETE FROM profesor WHERE id={$this->id}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}
}

?>