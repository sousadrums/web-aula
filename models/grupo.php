<?php 


class Grupo{

	private $id;
	private $grupo;
	private $id_tutor;
	private $id_aula;
	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}


	function getId(){
		return $this->id;
	}
	function getGrupo(){
		return $this->grupo;
	}

	function getId_tutor(){
		return $this->id_tutor;
	}

	function getId_aula(){
		return $this->id_aula;
	}

	function setId($id){
		$this->id=$id;
	}

	function setGrupo($grupo){
		$this->grupo=$grupo;
	}

	function setId_tutor($id_tutor){
		$this->id_tutor=$id_tutor;
	}

	function setId_aula($id_aula){
		$this->id_aula=$id_aula;
	}

	function grupoDesdeId(){
		$sql="SELECT g.grupo FROM grupo g WHERE id={$this->getId()}";
		$grupo= $this->db->query($sql)
		;
		$grup=$grupo->fetch_object();
		$string=$grup->grupo;
		return $string;
	}

	public function tutorGrupo(){
		$sql="UPDATE grupo SET id_tutor={$this->getId_tutor()} WHERE grupo='{$this->getGrupo()}'";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	function tutoria(){
		$id_aula=$this->getId_aula();
		$id_tutor=$this->getId_tutor();
		$grupo=$this->getGrupo();


		$sql="INSERT INTO asignatura  VALUES (NULL, 'tutoria', {$this->getId_tutor()}, {$this->getId()}, {$this->getId_aula()})";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function grupos(){
		$sql="SELECT * FROM grupo g  WHERE id_tutor=0 ORDER BY g.id ASC";
		$grupos= $this->db->query($sql);
		return $grupos;
	}

	public function grupoCon(){
		$sql="SELECT * FROM grupo g INNER JOIN profesor p ON  p.id=g.id_tutor WHERE p.rol!='admin' ORDER BY g.id ASC";
		$grupos= $this->db->query($sql);
		return $grupos;
	}
	

	function getAll(){
		$sql="SELECT * FROM profesor p RIGHT JOIN grupo g ON p.id=g.id_tutor WHERE rol!='admin'";
		$profesores= $this->db->query($sql);
		return $profesores;
	}

	public function save(){
		$sql = "INSERT INTO grupo VALUES(NULL, '{$this->getGrupo()}', {$this->getId_tutor()})";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function borrarGrupo(){
		$sql="DELETE FROM grupo WHERE id={$this->id}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}



} 
?>