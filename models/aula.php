<?php 


class aula{

	private $id;
	private $nombre;
	private $columnas;
	private $filas;
	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}


	function getId(){
		return $this->id;
	}
	function getNombre(){
		return $this->nombre;
	}
	function getColumnas(){
		return $this->columnas;
	}
	function getFilas(){
		return $this->filas;
	}

	function setId($id){
		$this->id=$id;
	}

	function setNombre($nombre){
		$this->nombre=$nombre;
	}

	function setColumnas($columnas){
		$this->columnas=$columnas;
	}

	function setFilas($filas){
		$this->filas=$filas;
	}

	function getPupitres(){
		$id=$this->getId();
		$pupitres= $this->db->query("SELECT * FROM aula WHERE id=$id ORDER BY id DESC;");
		return $pupitres;
	}

	function aulas(){
		$sql="SELECT * FROM aula";
		$aulas= $this->db->query($sql);
		return $aulas;
	}

	public function save(){
		$sql = "INSERT INTO aula VALUES (NULL, '{$this->getNombre()}', '{$this->getColumnas()}', '{$this->getFilas()}')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	function borrarAula(){
		$sql="DELETE FROM aula WHERE id={$this->id}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}





} 
?>