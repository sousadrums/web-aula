<?php


class asignatura{
	private $id;
	private $nombre;
	private $id_profesor;
	private $id_grupo;
	private $id_aula;



	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}


	function getId(){
		return $this->id;
	}

	function getNombre(){
		return $this->nombre;
	}

	function getId_profesor(){
		return $this->id_profesor;
	}

	function getId_grupo(){
		return $this->id_grupo;
	}

	function getId_aula(){
		return $this->id_aula;
	}

	function setId($id){
		$this->id=$id;
	}

	function setNombre($nombre){
		$this->nombre=$nombre;
	}

	function setId_profesor($id_profesor){
		$this->id_profesor=$id_profesor;
	}

	function setId_grupo($id_grupo){
		$this->id_grupo=$id_grupo;
	}

	function setId_aula($id_aula){
		$this->id_aula=$id_aula;
	}

	public function getAsignaturas(){
		$id_profesor=intval($this->getId_profesor());
		$sql="SELECT a.nombre asignatura, l.nombre aula, a.id id, g.grupo grupo, l.id id_aula FROM asignatura a INNER JOIN aula l ON a.id_aula=l.id  INNER JOIN grupo g ON a.id_grupo=g.id WHERE a.id_profesor=$id_profesor AND a.nombre<>'tutoria'";
		$asignaturas= $this->db->query($sql);
		return $asignaturas;
	}

	function crearAsignatura(){
		$sql="INSERT INTO asignatura VALUES(NULL, '{$this->getNombre()}', {$this->getId_profesor()}, {$this->getId_grupo()}, {$this->getId_aula()})";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}



}

?>